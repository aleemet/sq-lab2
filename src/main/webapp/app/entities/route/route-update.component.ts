import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IRoute } from 'app/shared/model/route.model';
import { RouteService } from './route.service';
import { ITrain } from 'app/shared/model/train.model';
import { TrainService } from 'app/entities/train';

@Component({
    selector: 'jhi-route-update',
    templateUrl: './route-update.component.html'
})
export class RouteUpdateComponent implements OnInit {
    route: IRoute;
    isSaving: boolean;

    trains: ITrain[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private routeService: RouteService,
        private trainService: TrainService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ route }) => {
            this.route = route;
        });
        this.trainService.query().subscribe(
            (res: HttpResponse<ITrain[]>) => {
                this.trains = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.route.id !== undefined) {
            this.subscribeToSaveResponse(this.routeService.update(this.route));
        } else {
            this.subscribeToSaveResponse(this.routeService.create(this.route));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRoute>>) {
        result.subscribe((res: HttpResponse<IRoute>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackTrainById(index: number, item: ITrain) {
        return item.id;
    }
}
