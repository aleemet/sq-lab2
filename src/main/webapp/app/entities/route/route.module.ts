import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Sq2SharedModule } from 'app/shared';
import {
    RouteComponent,
    RouteDetailComponent,
    RouteUpdateComponent,
    RouteDeletePopupComponent,
    RouteDeleteDialogComponent,
    RouteTimetablePopupComponent,
    RouteTimetableDialogComponent,
    routeRoute,
    routePopupRoute
} from './';

const ENTITY_STATES = [...routeRoute, ...routePopupRoute];

@NgModule({
    imports: [Sq2SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RouteComponent,
        RouteDetailComponent,
        RouteUpdateComponent,
        RouteTimetablePopupComponent,
        RouteTimetableDialogComponent,
        RouteDeleteDialogComponent,
        RouteDeletePopupComponent
    ],
    entryComponents: [
        RouteComponent,
        RouteUpdateComponent,
        RouteTimetablePopupComponent,
        RouteTimetableDialogComponent,
        RouteDeleteDialogComponent,
        RouteDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Sq2RouteModule {}
