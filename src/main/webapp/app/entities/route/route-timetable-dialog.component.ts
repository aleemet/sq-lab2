import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { IRoute } from 'app/shared/model/route.model';
import { RouteService } from './route.service';
import { ITimeTable } from '../../shared/model/time-table.model';
import { TimeTableService } from '../time-table/time-table.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import moment = require('moment');

@Component({
    selector: 'jhi-route-timetable-dialog',
    templateUrl: './route-timetable-dialog.component.html'
})
export class RouteTimetableDialogComponent implements OnInit {
    route: IRoute;
    timeTables: ITimeTable[];
    departureDate: any;

    constructor(
        private routeService: RouteService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private timeTableService: TimeTableService
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    ngOnInit() {
        this.timeTableService.timetablesForRoute(this.route.id).subscribe(
            (res: HttpResponse<ITimeTable[]>) => {
                this.timeTables = res.body;
                console.log('timetables', this.timeTables);
                this.departureDate = moment().format('YYYY-MM-DD');
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    confirmDelete(id: number) {
        this.routeService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'routeListModification',
                content: 'Deleted an route'
            });
            this.activeModal.dismiss(true);
        });
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}

@Component({
    selector: 'jhi-route-delete-popup',
    template: ''
})
export class RouteTimetablePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ route }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(RouteTimetableDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.route = route;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
