import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { Sq2RouteModule } from './route/route.module';
import { Sq2TrainModule } from './train/train.module';
import { Sq2StationModule } from './station/station.module';
import { Sq2TimeTableModule } from './time-table/time-table.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        Sq2RouteModule,
        Sq2TrainModule,
        Sq2StationModule,
        Sq2TimeTableModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Sq2EntityModule {}
