import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IStation } from 'app/shared/model/station.model';
import { Principal } from 'app/core';
import { StationService } from './station.service';

@Component({
    selector: 'jhi-station',
    templateUrl: './station.component.html'
})
export class StationComponent implements OnInit, OnDestroy {
    stations: IStation[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private stationService: StationService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.stationService.query().subscribe(
            (res: HttpResponse<IStation[]>) => {
                this.stations = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInStations();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IStation) {
        return item.id;
    }

    registerChangeInStations() {
        this.eventSubscriber = this.eventManager.subscribe('stationListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
