import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IStation } from 'app/shared/model/station.model';
import { StationService } from './station.service';

@Component({
    selector: 'jhi-station-update',
    templateUrl: './station-update.component.html'
})
export class StationUpdateComponent implements OnInit {
    station: IStation;
    isSaving: boolean;

    constructor(private stationService: StationService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ station }) => {
            this.station = station;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.station.id !== undefined) {
            this.subscribeToSaveResponse(this.stationService.update(this.station));
        } else {
            this.subscribeToSaveResponse(this.stationService.create(this.station));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IStation>>) {
        result.subscribe((res: HttpResponse<IStation>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
