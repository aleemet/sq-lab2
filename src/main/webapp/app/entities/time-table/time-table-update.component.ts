import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { ITimeTable } from 'app/shared/model/time-table.model';
import { TimeTableService } from './time-table.service';
import { IRoute } from 'app/shared/model/route.model';
import { RouteService } from 'app/entities/route';
import { IStation } from 'app/shared/model/station.model';
import { StationService } from 'app/entities/station';

@Component({
    selector: 'jhi-time-table-update',
    templateUrl: './time-table-update.component.html'
})
export class TimeTableUpdateComponent implements OnInit {
    timeTable: ITimeTable;
    isSaving: boolean;

    routes: IRoute[];

    stations: IStation[];
    arrival: string;
    departure: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private timeTableService: TimeTableService,
        private routeService: RouteService,
        private stationService: StationService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ timeTable }) => {
            this.timeTable = timeTable;
            this.arrival = this.timeTable.arrival != null ? this.timeTable.arrival.format(DATE_TIME_FORMAT) : null;
            this.departure = this.timeTable.departure != null ? this.timeTable.departure.format(DATE_TIME_FORMAT) : null;
        });
        this.routeService.query().subscribe(
            (res: HttpResponse<IRoute[]>) => {
                this.routes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.stationService.query().subscribe(
            (res: HttpResponse<IStation[]>) => {
                this.stations = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.timeTable.arrival = this.arrival != null ? moment(this.arrival, DATE_TIME_FORMAT) : null;
        this.timeTable.departure = this.departure != null ? moment(this.departure, DATE_TIME_FORMAT) : null;
        if (this.timeTable.id !== undefined) {
            this.subscribeToSaveResponse(this.timeTableService.update(this.timeTable));
        } else {
            this.subscribeToSaveResponse(this.timeTableService.create(this.timeTable));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ITimeTable>>) {
        result.subscribe((res: HttpResponse<ITimeTable>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackRouteById(index: number, item: IRoute) {
        return item.id;
    }

    trackStationById(index: number, item: IStation) {
        return item.id;
    }
}
