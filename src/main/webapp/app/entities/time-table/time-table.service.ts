import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITimeTable } from 'app/shared/model/time-table.model';

type EntityResponseType = HttpResponse<ITimeTable>;
type EntityArrayResponseType = HttpResponse<ITimeTable[]>;

@Injectable({ providedIn: 'root' })
export class TimeTableService {
    public resourceUrl = SERVER_API_URL + 'api/time-tables';

    constructor(private http: HttpClient) {}

    create(timeTable: ITimeTable): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(timeTable);
        return this.http
            .post<ITimeTable>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(timeTable: ITimeTable): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(timeTable);
        return this.http
            .put<ITimeTable>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ITimeTable>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ITimeTable[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    timetablesForRoute(id: number): Observable<EntityArrayResponseType> {
        return this.http
            .get<ITimeTable[]>(`${this.resourceUrl}/route/${id}`, { observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    private convertDateFromClient(timeTable: ITimeTable): ITimeTable {
        const copy: ITimeTable = Object.assign({}, timeTable, {
            arrival: timeTable.arrival != null && timeTable.arrival.isValid() ? timeTable.arrival.toJSON() : null,
            departure: timeTable.departure != null && timeTable.departure.isValid() ? timeTable.departure.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.arrival = res.body.arrival != null ? moment(res.body.arrival) : null;
        res.body.departure = res.body.departure != null ? moment(res.body.departure) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((timeTable: ITimeTable) => {
            timeTable.arrival = timeTable.arrival != null ? moment(timeTable.arrival) : null;
            timeTable.departure = timeTable.departure != null ? moment(timeTable.departure) : null;
        });
        return res;
    }
}
