import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { ITrain } from 'app/shared/model/train.model';
import { TrainService } from './train.service';
import { IRoute } from 'app/shared/model/route.model';
import { RouteService } from 'app/entities/route';

@Component({
    selector: 'jhi-train-update',
    templateUrl: './train-update.component.html'
})
export class TrainUpdateComponent implements OnInit {
    train: ITrain;
    isSaving: boolean;

    routes: IRoute[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private trainService: TrainService,
        private routeService: RouteService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ train }) => {
            this.train = train;
        });
        this.routeService.query({ filter: 'train-is-null' }).subscribe(
            (res: HttpResponse<IRoute[]>) => {
                if (!this.train.route || !this.train.route.id) {
                    this.routes = res.body;
                } else {
                    this.routeService.find(this.train.route.id).subscribe(
                        (subRes: HttpResponse<IRoute>) => {
                            this.routes = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.train.id !== undefined) {
            this.subscribeToSaveResponse(this.trainService.update(this.train));
        } else {
            this.subscribeToSaveResponse(this.trainService.create(this.train));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ITrain>>) {
        result.subscribe((res: HttpResponse<ITrain>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackRouteById(index: number, item: IRoute) {
        return item.id;
    }
}
