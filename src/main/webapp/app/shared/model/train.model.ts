import { IRoute } from 'app/shared/model//route.model';

export interface ITrain {
    id?: number;
    trainNumber?: number;
    route?: IRoute;
}

export class Train implements ITrain {
    constructor(public id?: number, public trainNumber?: number, public route?: IRoute) {}
}
