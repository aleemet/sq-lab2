import { Moment } from 'moment';
import { IRoute } from 'app/shared/model//route.model';
import { IStation } from 'app/shared/model//station.model';

export interface ITimeTable {
    id?: number;
    arrival?: Moment;
    departure?: Moment;
    route?: IRoute;
    station?: IStation;
}

export class TimeTable implements ITimeTable {
    constructor(public id?: number, public arrival?: Moment, public departure?: Moment, public route?: IRoute, public station?: IStation) {}
}
