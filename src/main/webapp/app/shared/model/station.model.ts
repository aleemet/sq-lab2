import { ITimeTable } from 'app/shared/model//time-table.model';

export interface IStation {
    id?: number;
    name?: string;
    timeTables?: ITimeTable[];
}

export class Station implements IStation {
    constructor(public id?: number, public name?: string, public timeTables?: ITimeTable[]) {}
}
