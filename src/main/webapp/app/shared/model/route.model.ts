import { ITimeTable } from 'app/shared/model//time-table.model';
import { ITrain } from 'app/shared/model//train.model';

export interface IRoute {
    id?: number;
    routeNumber?: number;
    timeTables?: ITimeTable[];
    train?: ITrain;
}

export class Route implements IRoute {
    constructor(public id?: number, public routeNumber?: number, public timeTables?: ITimeTable[], public train?: ITrain) {}
}
