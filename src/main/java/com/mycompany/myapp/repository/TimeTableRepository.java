package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.TimeTable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the TimeTable entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TimeTableRepository extends JpaRepository<TimeTable, Long> {

    @Query(value = "select * from time_table tt where tt.route_id = :id and cast(tt.departure as date) = current_date() order by arrival asc", nativeQuery = true)
    List<TimeTable> findByRouteAndAndDepartureIsToday(@Param("id") Long id);

}
