package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A TimeTable.
 */
@Entity
@Table(name = "time_table")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TimeTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "arrival", nullable = false)
    private Instant arrival;

    @NotNull
    @Column(name = "departure", nullable = false)
    private Instant departure;

    @NotNull
    @ManyToOne(optional = false)
    @JsonIgnoreProperties("timeTables")
    private Route route;

    @NotNull
    @ManyToOne(optional = false)
    @JsonIgnoreProperties("timeTables")
    private Station station;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getArrival() {
        return arrival;
    }

    public TimeTable arrival(Instant arrival) {
        this.arrival = arrival;
        return this;
    }

    public void setArrival(Instant arrival) {
        this.arrival = arrival;
    }

    public Instant getDeparture() {
        return departure;
    }

    public TimeTable departure(Instant departure) {
        this.departure = departure;
        return this;
    }

    public void setDeparture(Instant departure) {
        this.departure = departure;
    }

    public Route getRoute() {
        return route;
    }

    public TimeTable route(Route route) {
        this.route = route;
        return this;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Station getStation() {
        return station;
    }

    public TimeTable station(Station station) {
        this.station = station;
        return this;
    }

    public void setStation(Station station) {
        this.station = station;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TimeTable timeTable = (TimeTable) o;
        if (timeTable.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), timeTable.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TimeTable{" +
            "id=" + getId() +
            ", arrival='" + getArrival() + "'" +
            ", departure='" + getDeparture() + "'" +
            "}";
    }
}
